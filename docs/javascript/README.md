# JavaScript Interview Questions

Below is a list of sample interview questions.

## What are the different data types present in JavaScript?
- boolean - True or false
- string - Text
- numbers - Integers and decimals
- null - Nothing
- undefined - Not defined a value
- Symbol - Unique identify a value

#### Symbol
```javascript
const mySym = Symbol('CAT')
const myOtherSym = Symbol('CAT')
Symbol('CAT') === Symbol('CAT') // false
```

## Describe the difference between `var`, `let` and `const`

`var`
- Function scoped
- Attached to the window object
- Can be reassigned
- Is mutable

`let`
- Block scoped
- Can be reassigned
- Is mutable

`const`
- Block scoped
- Cannot be re-assigned 
- Objects and arrays are mutable

## Explain Coercion in javascript
- Implicit conversion of values of one data type to another.
- Values are coerced because JS is dynamically typed
- Coercion happens during comparison x == y or math operations (+,-,/,*)
- When ever + operator is used, JavaScript will attempt to coerce values to strings
```javascript
1 == '1' // Coerce to true
5 + '10' // 5 is Coerced to a string and result becomes '150'
```

## Difference between “==” and “===” operators.
-  == Abstract
   -  `1 == '1' // true`
-  === Strict comparison 
   -  `1 === '1' // false`
-  The `===` bypasses coercion for the comparison and validates the data type and the value

## What are some of the common loops in JavaScript
- for, for of, for in, and while loop and do-while
- Remember that do while is a post-test and while is a pre-test loop

## How can you loop through the keys of an object?
- Use `Object.keys()`
- Use a for...in loop
- for in (const) [property] in objects
- Also, when using for...in loop with arrays, it would give the index as the key

#### Using a for...in loop on an Object
```javascript
const romanNumerals = {
    'I': 1,
    'V': 5,
    'X': 10
}

// Using Object.keys()
const keys = Object.keys(romanNumerals) // ['I', 'V', 'X']

// Using the for...in
for (const key in romanNumerals) {
    console.log(key) // I, V, X
    console.log(romanNumerals[key]) // 1, 5, 10
}
```

## Is JavaScript a statically typed or a dynamically typed language?
- Dynamically typed because you can not declare type beforehand.
- Infers the type from the value
- TypeScript is a superset of JS that allows strongly typed variables in JavaScript

## What is the isNaN() function used for?
- Checking if something is a number. Returns true if a value is not a number.
- Applies coercion to strings 
  - `isNaN('10')` returns false

## What operator can you use to check the data type of a variable
- `typeof` operator 
- Returns a lowercase value describing the type i.e. `'string'`, `'number'`, `'boolean'`

```javascript
typeof "Hello World" // 'string'
typeof 22 // 'number'
typeof true // 'boolean'
```

## Explain passed by value and passed by reference.

Objects and arrays that are passed to functions as arguments are passed by reference. Any mutations made within the function will also apply to the object/array itself.
Primitive variables passed as arguments are passed by value. The variable will be copied, and mutations made to the variable in the function will only apply in the context of that function. Once the function terminates, the changes are lost.

- objects and arrays -> Always passed by reference
- primitives (strings, numbers, booleans) -> Always passed by value

#### Passed by Reference
```javascript
const referenceQuestion = { // Object
    text: 'A terrific question',
    answer: ''
}

function updateAnswer(question) {
    // question is a reference to referenceQuestion
    question.answer = 'Changed in Function'
}

updateAnswer(referenceQuestion) // Passed by Reference
console.log(referenceQuestion.answer) // 'Changed in Function'
```
#### Passed by Value
```javascript
let questionPoints = 5 // Primitive

function updatePoints(points) { 
    // points is Passed by Value, i.e. a copy
    points += 10 // Mutate the copy
    console.log(points) // 15
}

updatePoints(questionPoints) // Passed by Value
console.log(questionPoints) // 5
// Mutation in function only applies to argument (Copy of questionPoints)
```

## What is an Immediately Invoked Function in javascript?
It is a function that invokes itself. It does not need to be manually called in code.
- Often used with Module Pattern in JavaScript
  - Creates "protected" variables inside the scope of the IIFE
- Function should be anonymous
- Used to run async code as await keyword is not currently available in the global context
  
#### IIFE

```javascript
// IIFE with Module pattern
const messenger = (function() {
  let message = "Nice! I'm protected"

  return {
    getMessage() {
      return message
    }
  }
})() // IIFE

messenger.getMessage() // Nice! I'm protected

messenger.message // undefined, not accessible outside of IIFE.
```
#### IIFE to run async code.
```javascript
// IIFE to run async code
(async () => {
  // This scope now has await available.
  const response = await fetch('someurl')
})()
```
## Explain Higher Order Functions in javascript
- Higher order functions are functions passed as arguments that are often used in loops to process something for each iteration of the loop. 
- Higher Order functions may be anonymous or stored in a variable before being passed as an argument. 
- Commonly used HOFs are array methods like filter, map, reduce, find, findIndex etc.

#### Filter Higher Order Function

```javascript
const array = [10, 99, 5, 52, 9, 102, 33, 44]

// Anonymous HOF
const anonResult = array.filter(function(num) {
  return num > 50
})

// HOF as variable
const moreThanFifty = function(num) {
  return num > 50
}

const varResult = array.filter(moreThanFifty) // [ 99, 52, 102 ]
```

## What are some common Higher Order Functions on the array object and what are they used for?
All higher order functions are "non-destructive". I.e. Do not alter the array won which they are executed.

- `filter` - Filter out selected items in an array based on a true/false statement. May change the length of array
  - Returns an array
- `map` - Transform an array from one type to another. Length of the array can not change with map
  - Returns an array
- `reduce` - Reduce values in array to a single value
  - Returns mixed types
- `find` - Find the first element in an array based on a true return statement
  - Returns a element from the array or undefined
- `findIndex` - Find the index of an item in an array based on a true return statement
  - Returns a number
- `every` - Check if every item in an array meets a truthful condition
  - Returns true/false
- `some` - Check if some items in an array meets a truthful condition
  - Returns true/false

### Recommended reading:
[MDN Documentation on Arrays](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array#)

## Explain “this” keyword
`this` in JavaScript can refer to many different objects depending on the current execution context.

#### Global execution context
In the global execution context (top level of a JavaScript file), `this` refers to the Window object, i.e. the browser Window.

#### Function context
In a function context `this` refers to the context where the function was declared. With the exception of using the `use strict` where `this` will remain undefined.

#### Event Listener
In an event listener attached to an HTML element `this` refers to the Element that triggered the event.  With the exception that the event listener does not use an arrow function.

#### Object method
In a method attached to an object, this will refer to the object itself. 

#### Functional constructor
In a functional constructor `this` refers to the object that is created when an instance is created using the `new` keyword.

#### Class context
In the context of a class, when used in a method, `this` refers to the class itself.


### Recommended reading:
[MDN Documentation on this](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this)

## Explain Hoisting in JavaScript

> TLDR; You can use a function before it was declared in code. 

In JavaScript, hoisting refers to the way that JavaScript interprets and runs code. There are two phases in JavaScript, namely the Creation- and Execution Phase.

During the Creation Phase, JavaScript code is read line for line from top to bottom. Functions are NOT executed and assignment operators are ignored. Variables are placed in memory and assigned the value of `undefined`. This ignores any assignments manually given by the programmer. Therefor, all variables that are declared in the current execution scope will be `undefined`. Function code, although not executed are placed in memory along with the "placeholders" for variables. 

During the Execution Phase, the code is ran from top to bottom, this time Execution operations. Values are now assigned to variables and function are invoked. It should be noted that functions have their own Creation - and Execution phase. Because of the Creation phase, functions can be invoked before they are declared, as they are already stored in memory before the execution phase has started. 

Important to note, `let` and `const` are NOT hoisted. JavaScript will throw an error when trying to use a `let` or `const` before its declaration.

## Explain Scope and Scope Chain in JavaScript

Variables declared inside a function, may share the same name as a variable declared outside of the function. Arguments can also be named inside of function that share the same name as a variable in the global scope. This would, in the scope of that function, ignore the global variable and use the variable declared in the current scope instead.


#### Function scoped variable

```javascript
let outerName = 'I am outside'
function hackVariables() {
  let outerName = 'I am inside!'
  console.log(outerName) // I am inside
}
console.log(outerName) // I am outside.
```

#### Function scoped argument name
```javascript
let outerName = 'I am outside!'

function sameArgumentName(outerName) {
  console.log(outerName)
}

sameArgumentName('I am an argument') // Prints: I am an argument.
```

#### Scope Chain
The scope chain also allows access to variables outside of its own scope to be read. Because the variable name is not found in the scope of the function, it will check the scope in which the function was declared. It will continue this until it reaches the global scope. If the variable does not exist at that point, it would give an error.

```javascript
let outerValue = 'I live in the global scope'

function printVariables() {
  console.log(outerValue) // I live in the global scope.
}
```
## Explain Closures in JavaScript

A closure is a pattern created where a function returns a function. The function being returned can access the arguments of the outer function due to lexical scoping, even after the outer function has been invoked. Lexical scoping tells us, that the inner function can access variables of outer functions. 

#### Closure example

```javascript
function incrementCreator(incrementBy) {
  return function(valueToIncrement) {
    return valueToIncrement + incrementBy
  }
}
const incrementByFive = incrementCreator(5) // returns a function
const result = incrementByFive(10) // 15
```

### Recommended reading:
[MDN Documentation for Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)

## What are callbacks?

A callback function refers to a function that is passed as a parameter and executed by the function accepting is as an argument. Callback functions are typically anonymous, but can be declared.

#### Anonymous callback function
```javascript
setTimeout(function() { // callback function
  // Run some code after 1 second
}, 1000)

const scores = [10, 9, 6, 7, 8, 3, 3]
const failed = scores.filter(function(score) { // Callback function
  return score < 5
})
```

#### Declared callback function

```javascript
// Declared callback function
const print = function() {
  console.log('Time is up!')
}
setTimeout(print, 1000)

const scores = [10, 9, 6, 7, 8, 3, 3]
// Declared callback function
const filterFailures = score => score < 5
const failed = scores.filter(filterFailures)
```

## What is the use of a function constructor in JavaScript and what is a modern alternative?
A function constructor is a way to represent a real object in your code. Typical examples include a Person, Employee, Guitar, etc. A function constructor allows you to create a unique instance of the object without having to rewrite any of the properties or methods.

A modern JavaScript alternative would be to use the `class` syntax.

#### Function constructor

```javascript
// Function constructor syntax
function Guitar(model, manufacturer, colour) {
  this.model = model 
  this.manufacturer = manufacturer
  this.colour = colour
  this.getDetails = function() {
    return this.model + ' ' + this.manufacturer
  }
}
const fenderStrat = new Guitar('Stratocaster', 'Fender', 'Sunburst')
const gibsonLesPaul = new Guitar('Les Paul', 'Gibson', 'Black')
```
#### Class syntax
```javascript
class BassGuitar {
  constructor(model, manufacturer, colour) {
    this.model = model 
    this.manufacturer = manufacturer
    this.colour = colour
  }

  getDetails() {
    return this.model + ' ' + this.manufacturer
  }
}

const fenderJazz = new BassGuitar('Jazz', 'Fender', 'White')
const rickenbacker = new BassGuitar('4003S', 'Rickenbacker', 'Walnut')
```


## What is the DOM?
The Document Object Model refers to the HTML document that is represented and accessible in a programming language like JavaScript. Manipulation of the DOM using the interfaces in JavaScript will alter the structure and content of the HTML document displayed in the web application.

### Recommended reading: 
[MDN Documentation for DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Introduction)


## What is Currying in javascript? 
Curring is a design pattern in JavaScript where we can take a function with multiple arguments, and turn it into a function that returns a new function for each of the arguments. This is great for reusing code and composing new features with existing code.

#### Curring with filter

```javascript
// Array of names
const people = [
  { name: 'Thomas', age: 5 },
  { name: 'Steven', age: 22 },
  { name: 'Sarah', age: 25 },
  { name: 'Ahmed', age: 38 },
];

// Using currying to create a filter function
const filter = (fn) => (array) => array.filter(fn);

// Use the filter function to extend functionality
const filterBy = (property) => (value) => filter((item) => item[property] === value);

const filterGreaterThan = (prop) => (val) =>  filter((item) => item[prop] > val);

// Compose a filter by name and filter by name Sarah
const filterByName = filterBy('name');
const filterByNameSarah = filterByName('Sarah');

// Compose a filter by age greater than 20
const filterByAge = filterGreaterThan('age');
const filterByOlderThan20 = filterByAge(20);

// Use composed functions to get data from an array of objects
const sarahs = filterByNameSarah(people); // [{ name: 'Sarah' }]
const olderThan20 = filterByOlderThan20(people);
```


## What is an ES6 module and what benefits does it have?
An ES6 module allows developers to separate code into different files and import, selectively which code to use. The script tag MUST have the `type="module"` attribute set for the browser to understand modules. Module scripts are automatically deferred, so the `defer`keyword is not  required. Modules have top level await support. No need to wrap your code in an IIFE first. 

```html
<script type="module" src="js/index.js"></script>
```

Modules rely on the import/export syntax. 

#### Exporting features

```javascript
// modules/tools.js
export const createHammer = (name) => {
  return {
    type: 'Hammer',
    name
  }
}
```

#### Importing features
```javascript
// js/index.js
import { createHammer } from './modules/tools.js'
const thunderHammer = createHammer('Mjölnir')
```

Modules can be dynamically loaded using a Promise.


#### Dynamically load a module

```javascript
import('./modules/tools.js')
  .then(module => {
    // Module ready to use.
    module.createHammer('Mjölnir')
  })
```

### Recommended reading:
[MDN Documentation on Modules](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Modules)

## What is an arrow function and what does it do?

