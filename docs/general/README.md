# General Programming Questions

## What is recursion in a programming language?
Recursive programming is when a function is called within itself. Recursive functions relay on a return value and a terminator that ends the recursive call to avoid an infinite loop. This pattern is often used for algorithms.

## What does DRY mean in programming?
Don't Repeat Yourself. Following the DRY principle generally helps makes maintenance and updating of code easier.

## What are the SOLID principles
- S: Single-responsibility Principle
- O: Open-closed Principle
- L: Liskov Substitution Principle
- I: Interface Segregation Principle
- D: Dependency Inversion Principle

Recommended reading:
[freeCodeCamp on SOLID](https://www.freecodecamp.org/news/solid-principles-explained-in-plain-english/)

