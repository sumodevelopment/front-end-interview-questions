# React Interview Questions

## What is a component in React?

Components can be classes or functions, and are reusable pieces of code.

#### Function component


-	Functional Components can take a single optional argument (props)
-	Functional Components must return valid `JSX`
> Note: Functional components could not manage state before React 16.8. However, 16.8 and up can do so using hooks (useState / useEffect)

```jsx
const App = () => {
    return (
        <h4>Hello World!</h4>
    )
}
export default App
```

#### Class component

-	Class components must extend React’s “Component” and must have a render() function that returns JSX.
-	Class components can contain methods, and directly access lifecycles

```jsx
import React from 'react'

class App extends React.Component {
    
    constructor() {
        super()
    }

    componentDidMount() {
        // Did mount Lifecycle
    }

    render() {
        return (
            <h4>Hello World!</h4>
        )
    }
}
export default App
```

## What is JSX

JSX is an extension of JavaScript which allows developers to use HTML-like syntax to create UI elements. JSX is transpiled by an addition step using software like Babel to convert the JSX to HTML. 

A developer can embed JavaScript expressions using the a pair of curly braces `{}` to escape of the the JSX.

## Can browsers understand JSX?
No, JSX needs to be transpiled by an additional build step using a program like Babel.

## How do make a comment in JSX?
Escape the JSX by using the `{}` and place multiline comment.

```jsx
return (
    <div>
        { /* Hello, I am a JSX comment */ }    
        ...
    </div>
)

```

## What is the VirtualDOM
- In React, the Virtual DOM is a JavaScript representation of the DOM (Document Object Model) represented as "nodes and objects" that allows programming languages to interact with the page.
- When a JSX element renders, the Virtual DOM object updates, but React compares each virtual DOMs "state" before and after the update, and only applies the elements that have changed (called "DOM Diffing"). React then only re-renders the real DOMs that have changed. This saves time when making minor changes or interacting with a web page.

# What tool do you use to create a React application?
Create React App, which is a CLI that can generate a new React application. There are also flags available to access specific project templates, like setting up the project to use TypeScript or Redux.

## What is a prop in React?
A prop is a value that is passed down to a a child component. This value may be any valid data type in JavaScript. The syntax used is similar to defining an attribute on an HTML element. 

> Props are read-only and may NOT be changed in the component receiving the prop.

#### Using Props in React
```jsx
function List() {
    const [ list ] = useState(['Sleep', 'Eat', 'Code'])
    
    // Define item as the prop for ListItem
    return (
        <ul>
            <ListItem item={ list[0] } key="1" />
            <ListItem item={ list[1] } key="2" />
            <ListItem item={ list[2] } key="3" />
        </ul>
    )
}

function ListItem(props) {
    // Read the prop from the props object in the child component
    return (
        <li>{ props.item }</li>
    )
}
```

#### Props as functions

Props may also be provided a function as a value. The child component that receives that prop, may execute that function to "notify" the parent of events that have occurred in the child component.

```jsx
function List() {
    const [ list ] = useState(['Sleep', 'Eat', 'Code'])

    const handleItemClicked = () => {
        // Item was clicked.
    }
    
    // Define item as the prop as a function for ListItem
    return (
        <ul>
            <ListItem item={ list[0] } onItemClicked={ handleItemClicked } key="1" />
            ...
        </ul>
    )
}

function ListItem(props) {
    // Execute the prop onItemClicked when an item was clicked.
    return (
        <li onClick={ () => props.onItemClicked() }>{ props.item }</li>
    )
}
```


## What is an event in React?

React events are similar to regular DOM events with some syntactical differences. React events use camelCase and provide a SyntheticEvent which is a wrapper for the BrowserEvent. If you need access to the BrowserEvent, use the nativeElement attribute. 

### Recommended Reading:
- [React Handling Events](https://reactjs.org/docs/handling-events.html)
- [React SyntheticEvent](https://reactjs.org/docs/events.html)

## What are the most commonly used hooks in function components?

### useState
Define local state in your component. The useState returns an array with the first index being the state value and the second index being the state dispatch or setter.

### useEffect
The useEffect is used to simulate lifecycle hooks in function components and cause side effects based on changes in the state values. useEffect has two arguments, the callback function that should run and a dependency array for values to watch.


## How can you display a list of items in JSX and what are some requirements for displaying repeated items?
- Can display a list using the `.map()` method on an array/list and then return the items in the list
- Each item in the list must have a unique ID (key), can use the items index in the list if the list is “static” and won’t be reordered/changed. 
- It is discouraged to use the index as a key for multiple items
- Keys should remain the same between re-renders

## In React, components have relationships. Describe them and name some of the limitations.
- Components can have a parent - child relationship.
  - Data/state can be passed from parent to child using “props” and from child to parent with callbacks, 
- Components may also be siblings when nested at the same level. These components can not share local state with each other unless the state is "lifted" to a common parent.
- Alternatively, state may be shared between components using the ContextAPI or a state management solution like Redux.

## What is "lifting the state", what advantages does it have and name some disadvantages.
- Lifting the state refers to the practice of moving local state that must be shared to a common parent component. This parent component may then share the state with all its children using props.  
- Although the state is now shareable, all children components of the parent, event those not receiving the props will have to rerender when the state changes.

## What is the difference between a controlled and uncontrolled component?
- A controlled input refers to a form control that is linked to local state of a component. Controlled components cause its parent component to rerender on every keystroke or when a user changes the value of a select.
- An uncontrolled component refers to an input that is not linked to local state but may have a ref bound to it. 
- Uncontrolled components do not cause a component to rerender when the user changes the value of the input, select, etc.

### Recommended reading:
- [React Uncontrolled Component](https://reactjs.org/docs/uncontrolled-components.html)
- [React Forms](https://reactjs.org/docs/forms.html)

## How do you update the state of a component in React?
State in React components should always be updated using a dispatcher. When creating local state using the useState hook, the second index of the return value provides this dispatcher. 

When working in a class component, the setState method attached to the class should be used. 

In other scenarios where state management is used, the provided way to dispatch new state varies. 

```jsx
function List() {
    // setList is the dispatch 
    const [ list, setList ] = useState([])

    const handleAddToList = () => {
        // Use the dispatch function to update state
        setList([...list, 'new item!'])
    }
    ...
}
```

## Explain the useEffect hook and its arguments 
The `useEffect` hook is used to cause side effects in function components. The `useEffect` has 2 arguments, a callback function and a dependency array.

### useEffect callback
The callback function is the code that should be run when this `useEffect` is triggered. It may NOT be async. The function may have a return value that will be run when the component is unmounted from the DOM. This is useful when the `useEffect` has bound event listeners. i.e. onMouseMove etc.

The second argument for the `useEffect` is the dependency list. When any of these values change, the `useEffect` will execute on the next render. However, when this array is left empty, it will only execute the `useEffect` on the first render of the component. 

```jsx
function List() {

    const handleMouseMove = (event) => {
        console.log('Mouse moved!')
    }

    useEffect(() => {
        
        // Code to execute. 
        window.addEventListener('mousemove', handleMouseMove)

        return () => {
            // Runs when component is unmounted.
            window.removeEventListener('mousemove', () => {})
        }
    }, []) // Dependency list

}
```

Recommended reading:
- [React useEffect](https://reactjs.org/docs/hooks-effect.html)

## What is the ContextAPI in React?

The Context API in React is a way to create providers that can share state among unrelated components. This is a way to avoiding excessive prop-drilling. The recommendation is to use the Context API for state that does not change often. An example might be a light/dark theme setting.

### Recommended reading
- [React Context](https://reactjs.org/docs/context.html)
- [Prop Drilling - Kent C. Dodds](https://kentcdodds.com/blog/prop-drilling)

## What is a Higher Order Component in React?
A higher order component is a pattern inspired by the idea of a higher order function. We can use higher order components to wrap around other components and share functionality, styles and more.

### Recommended Reading 
[React Higher Order Components](https://reactjs.org/docs/higher-order-components.html)

## What does `props.children` refer to?

## Explain the process of setting up a project given to you by a colleague. Include from the start as if you do not have any code on your machine. 